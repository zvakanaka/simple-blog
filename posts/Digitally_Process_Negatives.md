# Digitally Process a Negative
## Photograph the negative with backlight 
![A negative sitting on a bright cell phone screen](https://c1.staticflickr.com/5/4345/37146189936_4b537c2ddd_z.jpg)  
## Crop
![The cropped negative](https://c1.staticflickr.com/5/4360/36499172404_ef0158f2bf.jpg)  
Thank Fred for the [NEGATIVE2POSITIVE script](http://www.fmwconcepts.com/imagemagick/negative2positive/index.php#t_zoomblur).   To use it, install [ImageMagick](https://www.imagemagick.org/script/index.php), download negative2positive, and run a command like this:  
```sh
negative2positive input.jpg output.jpg
```
![Digitally processed negative with accuratish color](https://c1.staticflickr.com/5/4350/36499171574_4bf74e04a9.jpg)
