# Simple Blog
Write in [markdown](https://www.markdowntutorial.com) and put your `.md` files in `/posts`.  

[demo](https://zvakanaka.github.io/simple-blog)
## Instructions
1. Put your articles in `/posts/`
2. Create an array of articles in `/posts.json`
 - manually, with the contents: `['file.md', 'next-file.md']`
 - automatically: `$ gulp nav`
3. Serve

## Dev Instructions
```sh
$ bower install
$ npm install
$ gulp
```
